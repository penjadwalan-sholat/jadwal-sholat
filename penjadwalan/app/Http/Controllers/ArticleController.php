<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class ArticleController extends Controller
{
    public function index1()
    {
        $articles = DB::table('articles')->get();
        return view('pages.artikel', ['articles' => $articles]);
    }

    public function create()
    {
        return view('artikel');
    }

 

    public function store(Request $request)
    {
    $request->validate([
        'image' => 'file|max:2048',
    ]);

    $tgl_terbit = $request->filled('tgl_terbit') ? Carbon::parse($request->tgl_terbit) : now();

    $imagePath = $request->file('image')->store('post-images');

    DB::table('articles')->insert([
        'judul' => $request->judul,
        'tgl_terbit' => $tgl_terbit,
        'image' => $imagePath,
        'isi' => $request->isi,
    ]);

    session()->flash('status', 'Artikel berhasil ditambahkan!');
    return redirect('artikel');
}


    public function show($id)
    {
        $articles = DB::table('articles')->find($id);
        return view('pages.artikel', compact('articles'));
    }

    public function edit($id)
    {
        $articles = DB::table('articles')->where('id', $id)->first();
        return view('pages.edit', compact('articles'));
    }

    public function editProcces(Request $request, $id)
{
    $request->validate([
        'image' => 'file|max:2048',
    ]);

    $tgl_terbit = $request->filled('tgl_terbit') ? $request->tgl_terbit : now();

    if ($request->hasFile('image')) {
        $imagePath = $request->file('image')->store('post-images');

        DB::table('articles')->where('id', $id)->update([
            'judul' => $request->judul,
            'tgl_terbit' => $tgl_terbit,
            'image' => $imagePath,
            'isi' => $request->isi,
        ]);
    } else {
        DB::table('articles')->where('id', $id)->update([
            'judul' => $request->judul,
            'tgl_terbit' => $tgl_terbit,
            'isi' => $request->isi,
        ]);
    }

    session()->flash('edit', 'Artikel berhasil diupdate!');
    return redirect()->route('artikel');
}

    public function showBerita()
    {
        $articles = DB::table('articles')->latest()->take(3)->get();
        return view('welcome', compact('articles'));
    }



    public function update(Request $request, $id)
    {
        // Metode update dapat diimplementasikan jika diperlukan
    }

    public function delete($id)
    {
        DB::table('articles')->where('id', $id)->delete();
        session()->flash('success', 'Artikel berhasil dihapus!');
        return redirect('artikel');
    }
}