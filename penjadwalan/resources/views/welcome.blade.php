<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Sholat Reminder</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

  <!-- 
    - favicon
  -->
  <link rel="website icon" type="png" href="Master/assets/images/masjid.png">


  <!-- 
    - custom css link
  -->
  <link rel="stylesheet" href="Master/assets/css/style.css">

  <!-- 
    - google font link
  -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@700&family=Roboto:wght@400;700&family=Saira+Stencil+One&display=swap" rel="stylesheet">
</head>

<body id="top" class="dark_theme">

  <!-- 
    - #HEADER
  -->

  <!-- ... (previous code) ... -->

  <header class="header" data-header>
    <div class="container">

      <h1 class="h1 logo">
        <a href="#">Sholat<span>Remainder</span>.</a>
      </h1>

      <div class="navbar-actions">
        <button class="theme-btn" aria-label="Change Theme" title="Change Theme" data-theme-btn>
          <span class="icon"></span>
        </button>

      </div>

      <button class="nav-toggle-btn" aria-label="Toggle Menu" title="Toggle Menu" data-nav-toggle-btn>
        <span class="one"></span>
        <span class="two"></span>
        <span class="three"></span>
      </button>

      <nav class="navbar" data-navbar>
        <ul class="navbar-list">
          <li>
            <a href="#home" class="navbar-link">Home.</a>
          </li>
          <li>
            <a href="#about" class="navbar-link">Jadwal Sholat.</a>
          </li>
          <li>
            <a href="#Berita" class="navbar-link">Berita.</a>
          </li>
        </ul>
      </nav>

    </div>
  </header>

  <!-- ... (remaining code) ... -->


  <main>
    <article class="container">

      <!-- 
        - #HERO
      -->

      <section class="hero" id="home">

        <figure class="hero-banner">
          <picture>
            <source srcset="Master/assets/images/masjid1.png" media="(min-width: 768px)">
            <source srcset="Master/assets/images/masjid1.png" media="(min-width: 500px)">
            <img src="Master/assets/images/masjid1.png" alt="A man in a blue shirt with a happy expression" class="w-100">
          </picture>
        </figure>
        <div class="hero-content">
          <h2 class="h2 hero-title">Welcome To SHOLATREMAINDER</h2>
          <p class="h4 hero-p"><br>"Jangan Pernah Melewatkan Ibadah Dan Shalat.
            Karena Ada Jutaan Manusia Di Alam Kubur Yang Ingin Dihidupkan KembaliHanya
            Untuk Beribadah Dan Bisa Sholat Kembali"
            </br></p>
        </div>

        <a href="#stats" class="scroll-down">home</a>

      </section>



      <!-- Jadwal Sholat Section -->
      <section class="about" id="about">
        
      </section>
      <!-- 
        - #Berita
      -->
      <section class="berita" id="Berita">
        <div class="container">
          <div class="berita-content section-content">
            <h2 class="h3 section-title">Latest News.</h2>
            <div class="row">
              <div class="card-deck mb-4">
                @php
                    $articles = DB::table('articles')->get();
                @endphp
            
                @foreach($articles as $article)
                    <div class="card">
                        <img src="{{ $article->image }}" class="card-img-top" alt="{{ $article->judul }}">
                        <div class="card-body">
                            <h5 class="card-title">{{ $article->judul }}</h5>
                            <p>{{ \Carbon\Carbon::parse($article->tgl_terbit)->format('d F Y H:i') }}</p>
            
                            <p class="card-text">{{ substr($article->isi, 0, 200) . '...' }}</p>
            
                            <a href="#" class="btn btn-primary">Baca Selengkapnya</a>
                        </div>
                    </div>
                @endforeach
            </div>
            </div>
          </div>
        </div>
      </section>

      <!-- Modal -->
      <div class="modal fade" id="myModal1">
        <div class="modal-dialog">
          <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Mengungkap Misteri Awal Alam Semesta: Big Bang</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal Body -->
            {{-- <div class="modal-body">
              <p>20 desember 2019 15.13 Wib </p>
              <img src="Master/assets/images/big.png" class="img-fluid mb-2" alt="Headline Berita">
              <p>Big Bang, atau ledakan besar, adalah teori dominan yang menjelaskan asal usul Alam Semesta. Teori ini menyajikan gambaran bahwa sekitar 13,8 miliar tahun yang lalu, seluruh materi dan energi yang membentuk Alam Semesta ini berkumpul dalam satu titik kepadatan dan suhu yang luar biasa tinggi. Sejak saat itu, Alam Semesta terus berkembang dan muncul dalam bentuk yang kita kenal hari ini. Mari kita eksplorasi lebih dalam mengenai konsep menarik ini.
                Proses Pembentukan:
                Menurut teori Big Bang, semua materi di Alam Semesta berasal dari satu titik kepadatan dan suhu ekstrem yang disebut sebagai "singularitas." Pada saat itu, dimensi ruang dan waktu seperti yang kita kenal belum ada.
                Secara tiba-tiba, terjadi ledakan besar yang memulai perluasan Alam Semesta. Partikel-partikel subatom mengalami transformasi menjadi proton, neutron, dan elektron.
                Pembuktian Observasional:
                Salah satu bukti utama dari teori Big Bang adalah radiasi latar belakang gelombang mikro kosmik, yang merupakan sisa panas yang dihasilkan selama Big Bang. Radiasi ini pertama kali ditemukan pada tahun 1965 oleh Arno Penzias dan Robert Wilson.
                Pembentukan Unsur dan Galaksi:
                Selama beberapa menit setelah Big Bang, suhu dan tekanan sangat tinggi sehingga terjadi nukleosintesis, yang menghasilkan unsur-unsur ringan seperti hidrogen, helium, dan sedikit litium.
                Selama miliaran tahun berikutnya, materi ini berkumpul membentuk galaksi, bintang, dan planet.
                Perkembangan dan Perluasan:
                Setelah Big Bang, Alam Semesta terus berkembang dan melaju ke arah yang tidak terbatas. Observasi dari galaksi jauh menunjukkan bahwa perluasan ini masih berlanjut hingga saat ini.
                Teori Inflasi:
                Untuk menjelaskan beberapa fenomena yang tidak dapat dijelaskan oleh teori Big Bang klasik, para ilmuwan mengajukan konsep inflasi. Teori ini menyatakan bahwa pada fase awal perluasan Alam Semesta terjadi dengan kecepatan yang sangat cepat.
                Kesimpulan:
                Big Bang bukan hanya sekadar teori, tetapi juga dasar pemahaman kita tentang asal muasal Alam Semesta. Melalui pengamatan dan penelitian yang mendalam, ilmuwan terus mencoba memahami rahasia dan misteri di balik kejadian fenomenal ini. Dengan setiap penemuan baru, kita semakin mendekati pemahaman yang lebih baik tentang bagaimana kita dan Alam Semesta ini bermula.</p>
            </div> --}}
            <!-- Modal Footer -->
            {{-- <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
            </div> --}}
          </div>
        </div>
      </div>
      <div class="modal fade" id="myModal2">
        <div class="modal-dialog">
          <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Perang Uhud: Epik dan Tantangan dalam Sejarah Islam</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
              <p>20 desember 2019 15.13 Wib </p>
              <img src="Master/assets/images/uhut.png" class="img-fluid mb-2" alt="Headline Berita">
              <p>Perang Uhud adalah salah satu peristiwa penting dalam sejarah Islam yang terjadi pada tahun 625 Masehi, di Madinah. Perang ini melibatkan pasukan Muslim yang dipimpin oleh Nabi Muhammad SAW dan pasukan Quraisy dari Mekah. Sebagai kelanjutan dari Pertempuran Badar, Perang Uhud menyajikan serangkaian tantangan dan pembelajaran yang mendalam bagi umat Islam.
                Latar Belakang Pertempuran:
                Setelah kemenangan di Pertempuran Badar, umat Islam menghadapi oposisi yang semakin intens dari suku Quraisy. Mekah berusaha membalas kekalahan mereka dan memulihkan kehormatan yang terguncang.
                Tantangan Strategis:
                Pasukan Muslim yang berjumlah lebih sedikit dari pasukan musuh memutuskan untuk mempertahankan kota Madinah di Bukit Uhud. Meskipun strategi ini awalnya berhasil, namun beberapa pasukan Muslim meninggalkan posisi mereka, mengakibatkan celah dalam barisan pertahanan.
                Peristiwa Kunci:
                Pertempuran Uhud menyaksikan peristiwa dramatis ketika pasukan Muslim awalnya berhasil mengatasi pasukan Quraisy. Namun, ketika sebagian pasukan Muslim meninggalkan posisi mereka untuk mengejar rampasan perang, pasukan Quraisy memanfaatkan kesempatan tersebut dan membalikkan keadaan.
                Pengorbanan Sahabat:
                Perang Uhud mencatat pengorbanan besar dari beberapa sahabat Nabi, termasuk Hamzah bin Abdul Muttalib, paman Nabi, yang gugur dalam pertempuran ini.
                Hikmah dan Pembelajaran:
                Meskipun pasukan Muslim mengalami kerugian dalam Perang Uhud, peristiwa ini memberikan banyak hikmah dan pembelajaran. Umat Islam belajar tentang pentingnya ketaatan terhadap perintah pemimpin, disiplin dalam melaksanakan strategi, dan kesabaran dalam menghadapi cobaan.
                Kesimpulan:
                Perang Uhud tidak hanya menjadi catatan bersejarah tentang peristiwa militer, tetapi juga mengajarkan umat Islam tentang nilai-nilai kesetiaan, kedisiplinan, dan kesabaran. Meskipun menghadapi tantangan, peristiwa ini memperkuat tekad umat Islam dalam menegakkan kebenaran dan melanjutkan perjuangan mereka untuk mempertahankan agama dan nilai-nilai Islam. Perang Uhud menjadi simbol keberanian dan semangat perjuangan umat Islam dalam menghadapi rintangan dan ujian hidup.</p>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="myModal3">
        <div class="modal-dialog">
          <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Kemegahan Kerajaan Utsmaniyah: Puncak Kekuasaan di Tengah Dunia Islam</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal Body -->
            <div class="modal-body">
              <p>20 desember 2019 15.13 Wib </p>
              <img src="Master/assets/images/usmani.png" class="img-fluid mb-2" alt="Headline Berita">
              <p>Kerajaan Utsmaniyah, juga dikenal sebagai Kesultanan Utsmaniyah, merupakan salah satu dinasti terbesar dan paling berpengaruh dalam sejarah dunia Islam. Puncak kejayaan Kerajaan Utsmaniyah dicapai selama masa pemerintahan Kesultanan Utsmaniyah yang disebut sebagai masa pemerintahan Utsmaniyah.
                Asal Usul Kesultanan Utsmaniyah:
                Kesultanan Utsmaniyah didirikan oleh Osman I pada akhir abad ke-13 dan terus berkembang di bawah kepemimpinan keturunannya, terutama pada masa Sultan Selim I dan Sultan Suleiman I.
                Pembentukan Kekuatan Militer:
                Salah satu kunci keberhasilan Kesultanan Utsmaniyah adalah pembentukan kekuatan militer yang kuat. Pasukan Utsmaniyah terkenal dengan keahlian mereka dalam menggunakan senjata api baru dan formasi militer yang canggih.
                Ekspansi Wilayah:
                Kesultanan Utsmaniyah mengalami pertumbuhan wilayah yang signifikan. Di bawah kepemimpinan Sultan Selim I, mereka menaklukkan Mesir, Hijaz, dan sebagian besar wilayah Timur Tengah. Sultan Suleiman I, yang dikenal sebagai Suleiman yang Agung, memperluas kekuasaan Utsmaniyah hingga ke Eropa tenggara, Afrika Utara, dan bagian dari Eropa tengah.
                Puncak Kemegahan di Bidang Seni dan Kebudayaan:
                Selama masa kejayaannya, Kesultanan Utsmaniyah mencapai puncak kemegahan seni dan kebudayaan Islam. Seni arsitektur Utsmaniyah tercermin dalam megahnya masjid-masjid, istana, dan bangunan-bangunan monumental seperti Masjid Sultan Ahmed dan Istana Topkapi di Istanbul.
                Sistem Pemerintahan Efisien:
                Kesultanan Utsmaniyah mengembangkan sistem pemerintahan yang efisien dengan membagi wilayah ke dalam provinsi-provinsi dan memberikan otonomi terbatas pada penguasa setempat. Sistem ini memberikan stabilitas politik dan administratif.
                Kesimpulan:
                Kemegahan Kerajaan Utsmaniyah selama masa pemerintahan Utsmaniyah menciptakan warisan bersejarah yang mempengaruhi banyak aspek kehidupan di dunia Islam. Dengan kombinasi kekuatan militer yang tangguh, ekspansi wilayah yang luas, dan kemajuan seni serta kebudayaan, Kesultanan Utsmaniyah mencapai puncak kejayaannya dan memberikan kontribusi besar terhadap peradaban dunia Islam. Meskipun kemudian mengalami penurunan, kejayaan Kerajaan Utsmaniyah tetap menjadi bagian integral dari sejarah dan warisan kultural di wilayah tersebut.</p>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
            </div>
          </div>
        </div>
      </div>


      <!-- 
    - #FOOTER
  -->

      <footer class="footer">
        <div class="container">

          <p class="h1 logo">
            <a href="#">
              Sholat<span>Reminder</span>
            </a>
          </p>

          <p class="copyright">
            &copy; 2023 <a href="#">Kelompok 4</a>. All rights reserved
          </p>

        </div>
      </footer>








      <!-- 
    - custom js link
  -->
      <script src="Master/assets/js/script.js"></script>

      <!-- 
    - ionicon link
  -->
      <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
      <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.0.7/dist/umd/popper.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>

</html>