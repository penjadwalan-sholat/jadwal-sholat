@extends('layouts.app')

@section('content')
@include('layouts.navbars.auth.topnav', ['title' => ' Create Artikel'])
<div class="row mt-4 mx-4">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-header pb-0">
                <h6><strong>Input Artikel Baru</strong></h6>
            </div>
            
            <div class="card-body pb-2">
                <div class="table-responsive p-0">
                    <form class="form-horizontal" action="/artikel" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="" class="form-label">Judul:</label>
                            <input type="text" class="form-control" name="judul" placeholder="Masukkan Judul Artikel" autofocus required>
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Tanggal Terbit:</label>
                            <input type="date" class="form-control" name="tanggal" placeholder="tanggal terbit artikel" autofocus required>
                        </div>
                        <div class="form-group">
                            <label for="image" class="form-label">Gambar:</label>
                            <input class="form-control" type="file" id="image" name="image">
                          </div>
                        <div class="form-group">
                            <label for="" class="form-label">Isi:</label>
                            <textarea name="isi" class="form-control" placeholder="Berita Terbaru Hari ini" rows="5"></textarea>
                        </div>
            
                        <div class="nav justify-content-end">
                            <a href="{{ url('artikel/') }}" class="btn btn-secondary">Cancel</a>
                            <button class="btn btn-success">Tambah</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection