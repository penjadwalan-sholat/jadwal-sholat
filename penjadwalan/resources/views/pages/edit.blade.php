@extends('layouts.app')

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => 'Edit Artikel'])
    <div class="row mt-4 mx-4">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6><strong>Halaman Edit Artikel Sholat Reminder</strong></h6>
                </div>
                <div class="card-body pb-2">
                    <div class="table-responsive p-0">
                        <form action="{{url('artikel/'.$articles->id)}}" method="post" enctype="multipart/form-data">
                            @method('patch')
                            @csrf
                            
                            <div class="form-group">
                                <label for="" class="form-label">Judul</label>
                                <input type="text" class="form-control" name="judul" placeholder="Masukkan Judul" value="{{ $articles->judul }}">
                            </div>
                            <div class="form-group">
                                <label for="" class="form-label">Tanggal Terbit</label>
                                <input type="date" class="form-control" name="tanggal" value="{{ $articles->tgl_terbit }}">
                            </div>
                            <div class="form-group">
                                <label for="image" class="form-label">Gambar</label>
                                <input type="file" class="form-control" id="image" name="image" value="{{ $articles->image }}">
                            </div>
                            <div class="form-group">
                                <label for="content" class="form-label">Artikel</label>
                                <textarea name="content" class="form-control" rows="5" placeholder="Masukkan isi artikel...">{{ $articles->isi }}</textarea>
                            </div>
                
                            <div class="nav justify-content-end">
                                <a href="/artikel" class="btn btn-secondary">Cancel</a>
                                <button class="btn btn-success">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
