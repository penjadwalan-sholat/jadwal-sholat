@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => 'Data Artikel'])
    @include('layouts.footers.auth.footer')

    <div class="container-fluid py-4" >
        @if(session('success'))
        <div class="alert alert-danger">{{ session('success') }}</div>
        <script type="text/javascript">
            setTimeout(function() {
                document.querySelector('.alert').style.display = 'none';
            }, 5000);
        </script>
        @endif
        @if (session('status'))
        <div class="alert alert-success">{{session('status')}}</div> 
        <script type="text/javascript">
            setTimeout(function() {
                document.querySelector('.alert').style.display = 'none';
            }, 5000);
        </script>       
        @endif
        @if (session('edit'))
        <div class="alert alert-warning">{{session('edit')}}</div>   
        <script type="text/javascript">
            setTimeout(function() {
                document.querySelector('.alert').style.display = 'none';
            }, 5000);
        </script>     
        @endif
        <div class="card mb-4 card-header pb-0"">
            <h5><strong>Data Artikel Sholat Reminder</strong></h5>
            <div class="table-responsive" style="width: 100%; overflow-x: auto;">
                <div class="text-right mb-2">
                    <a href="/create" class="btn btn-info" title="Tambah Artikel Baru">+ Add</a>
                </div>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center text-uppercase text-secondary text-xs font-weight-bolder opacity-7">No</th>
                            <th class="text-center text-uppercase text-secondary text-xs font-weight-bolder opacity-7">Judul</th>
                            <th class="text-center text-uppercase text-secondary text-xs font-weight-bolder opacity-7">Tanggal Terbit</th>
                            <th class="text-center text-uppercase text-secondary text-xs font-weight-bolder opacity-7">Content</th>
                            <th class="text-center text-uppercase text-secondary text-xs font-weight-bolder opacity-7">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $articles = DB::table('articles')->get();
                        @endphp
                        @foreach($articles as $article)
                            <tr>
                                <td class="text-xs" style="word-wrap: break-word; white-space: normal;">{{ $loop->iteration }}</td>
                                <td class="text-xs" style="word-wrap: break-word; white-space: normal;">{{ $article->judul }}</td>
                                <td class="text-xs" style="word-wrap: break-word; white-space: normal;">{{ $article->tgl_terbit }}</td>
                                <td class="text-xs" style="word-wrap: break-word; white-space: normal;">{{ substr($article->isi, 0, 300) }}</td>
                                <td>
                                    <a href="{{url('artikel/'.$article->id) }}" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#myModal-{{ $article->id }}">
                                        <i class="fas fa-eye" title="Show"></i>
                                    </a>
                                    <a href="{{url('edit/'.$article->id) }}" class="btn btn-secondary btn-sm">
                                        <i class="fas fa-edit" title="Edit"></i>
                                    </a>
                                    
                                    <form id="delete-form-{{ $article->id }}" class="d-inline" method="post" action="{{ url('artikel/' . $article->id) }}" onsubmit="return false;">
                                        @method('delete')
                                        @csrf
                                        <a href="#" class="btn btn-danger btn-sm" onclick="deleteConfirmation({{ $article->id }})">
                                            <i class="fa fa-trash" title="Delete"></i>
                                        </a>
                                    </form>     
                                </td>
                            </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal-{{ $article->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Preview Artikel</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-xs">
                    <img src="{{ asset('storage/' . $article->image) }}" alt="Artikel Image" class="img-fluid mb-2" />
                    <ul class="list-unstyled text-left">
                        <li><strong>Judul :</strong>&nbsp;{{ $article->judul }}</li>
                        <li><strong>Tanggal Terbit :</strong>&nbsp;{{ $article->tgl_terbit }}</li>
                        <li><strong>Isi :</strong>&nbsp;{{  $article->isi }}</li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection