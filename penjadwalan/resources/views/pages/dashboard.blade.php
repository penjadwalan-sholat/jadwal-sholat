@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'Jadwal Sholat'])
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>JADWAL SHOLAT</h6>
                </div>
                <div class="d-flex justify-content-between align-items-center px-4">
                    <input type="date" id="inputDate" placeholder="Pilih Tanggal" class="mr-2">
                    <input type="text" id="inputSearch" placeholder="Cari nama kota..." class="ml-2">
                </div>
                <div class="text-center mt-4">
                    <div id="digitalClock">12:00:00 AM</div>
                </div>
                <table id="example1" class="table table-bordered table-striped mt-4">
                    <thead>
                        <tr>
                            <th style="text-align: center;">Subuh</th>
                            <th style="text-align: center;">Dzuhur</th>
                            <th style="text-align: center;">Ashar</th>
                            <th style="text-align: center;">Maghrib</th>
                            <th style="text-align: center;">Isha</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    function updateClock() {
        const digitalClock = document.getElementById("digitalClock");
        const now = new Date();
        const hours = now.getHours().toString().padStart(2, "0");
        const minutes = now.getMinutes().toString().padStart(2, "0");
        const seconds = now.getSeconds().toString().padStart(2, "0");
        const ampm = hours >= 12 ? "PM" : "AM";
        const formattedTime = `${hours}:${minutes}:${seconds} ${ampm}`;
        digitalClock.textContent = formattedTime;
    }

    setInterval(updateClock, 1000);

    updateClock();
</script>
@endsection